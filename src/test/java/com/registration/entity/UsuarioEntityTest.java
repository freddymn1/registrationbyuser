package com.registration.entity;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.registration.dto.Phone;
import com.registration.dto.UserRequest;
import com.registration.exception.BusinessException;
import com.registration.repository.RegistrationUserRepository;
import com.registration.service.RegistrationUserService;

@RunWith(MockitoJUnitRunner.class)
public class UsuarioEntityTest {

	@Mock
	RegistrationUserRepository registrationUserRepository;

	@InjectMocks
	@Spy
	RegistrationUserService registrationUserService;

	@Test
	public <T> void validaEmail() {

		UsuarioEntity userEntity = new UsuarioEntity();
		userEntity.setEmail("fopg@gmail.org");

		assertThat((T) registrationUserRepository.existsByEmail(userEntity.getEmail())).isNotNull();

	}

	@Test
	public <T> void buscaUsuario() {

		UsuarioEntity userEntity = new UsuarioEntity();
		userEntity.setName("fopg");
		userEntity.setPassword("12Fiwe");
		assertThat((T) registrationUserRepository.existsByName(userEntity.getName(), userEntity.getPassword()))
				.isNotNull();

	}

	@Test
	public void registrarUsuarioTest() throws ParseException {

		UserRequest userEntity = new UserRequest();
		Phone phone = new Phone();
		List<Phone> phones = new ArrayList<Phone>();

		phone.setCityCode("12");
		phone.setContryCode("51");
		phone.setNumber("51938290");
		phones.add(phone);
		userEntity.setName("fopg");
		userEntity.setPassword("12Fiwe");
		userEntity.setEmail("fopg@gmail.org");
		userEntity.setPhones(phones);

		try {
			registrationUserService.saveUpUser(userEntity);
		} catch (BusinessException e) {
			assert (Boolean.TRUE);
		}

	}

}
