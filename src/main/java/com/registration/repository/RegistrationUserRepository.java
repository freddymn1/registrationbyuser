package com.registration.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.registration.entity.UsuarioEntity;

@Repository
@Transactional
public interface RegistrationUserRepository extends CrudRepository<UsuarioEntity, Long> {

	@Query("SELECT CASE WHEN COUNT(u) > 0 THEN 'true' ELSE 'false' END FROM UsuarioEntity u WHERE u.email = ?1")
	public Boolean existsByEmail(String email);

	@Query("SELECT CASE WHEN COUNT(u) > 0 THEN 'true' ELSE 'false' END FROM UsuarioEntity u WHERE u.name = ?1 AND u.password = ?2 ")
	public Boolean existsByName(String name, String password);

	@Modifying(clearAutomatically = true)
	@Query("SELECT u FROM UsuarioEntity u WHERE u.name = ?1 AND u.password = ?2 ")
	public List<UsuarioEntity> findUser(String name, String password);
}
