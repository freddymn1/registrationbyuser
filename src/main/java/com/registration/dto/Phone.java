package com.registration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "number", "citycode","contrycode"})
@Getter
@Setter
@ToString
public class Phone {

	@JsonProperty("number")
	private String number;
	@JsonProperty("citycode")
	private String cityCode;
	@JsonProperty("contrycode")
	private String contryCode;
}
