
package com.registration.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "email", "phones" })
@Getter
@Setter
@ToString
public class UserRequest {

	@JsonProperty("name")
	@NotBlank(message = "Nombre es requerido")
	private String name;
	@NotBlank(message = "Email es requerido")
	@JsonProperty("email")
	@Pattern(regexp = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$", message = "Email no Valido")
	private String email;
	@JsonProperty("password")
	@NotBlank(message = "Clave es requerido")
	@Pattern(regexp = "([A-Z]{1}[a-z]*[0-9]{2})|([a-z]*[A-Z]{1}[0-9]{2})|([0-9]{2}[A-Z]{1}[a-z]*)|([0-9]{2}[a-z]*[A-Z]{1})|([a-z]*[0-9]{2}[A-Z]{1})$", message = "Una Mayúscula, letras minúsculas, y dos números")
	private String password;
	@JsonProperty("phones")
	private List<Phone> phones;

}
