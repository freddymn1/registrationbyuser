package com.registration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AppResponse {

	@JsonProperty("id")
	private String id;

	@JsonProperty("created")
	private String created;

	@JsonProperty("modified")
	private String modified;

	@JsonProperty("last_login")
	private String last_login;

	@JsonProperty("token")
	private String token;

	@JsonProperty("isactive")
	private String isactive;

}
