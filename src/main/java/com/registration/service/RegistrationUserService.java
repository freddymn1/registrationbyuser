package com.registration.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.registration.dto.AppResponse;
import com.registration.dto.Phone;
import com.registration.dto.RegistrationUserResponse;
import com.registration.dto.UserRequest;
import com.registration.entity.PhoneEntity;
import com.registration.entity.UsuarioEntity;
import com.registration.exception.BusinessException;
import com.registration.repository.RegistrationUserRepository;
import com.registration.utils.Constants;
import com.registration.utils.ErrorEnumUtil;
import com.registration.utils.InfoEnumUtil;
import com.registration.utils.LogMessageUtil;
import com.registration.utils.Utils;

/**
 * Servicio para registrar Usuario en BD H2
 * 
 * @author fpainenao
 *
 */

@Service
public class RegistrationUserService {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationUserService.class);

	@Autowired(required = true)
	RegistrationUserRepository registrationUserRepository;

	/**
	 * registra Usuario en modelo H2
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public RegistrationUserResponse saveUpUser(UserRequest user) throws ParseException {
		List<PhoneEntity> listaPhoneEntity = new ArrayList<PhoneEntity>();
		RegistrationUserResponse response = new RegistrationUserResponse();
		AppResponse data = new AppResponse();

		SimpleDateFormat dt1 = new SimpleDateFormat(Constants.FORMATO_FECHA);
		logger.info("RegistrationUserService input {}", InfoEnumUtil.INI_REGISTRA_USUARIO_SERVICE.getCode());

		try {

			if (registrationUserRepository.existsByEmail(user.getEmail())) {
				response.setMessage(Constants.MAIL_ALREADY_REGISTERED);
				response.setStatus(Constants.STATUS_FAIL);
				return response;
			}
			UsuarioEntity usuarioEntity = new UsuarioEntity();
			usuarioEntity.setName(user.getName());
			usuarioEntity.setPassword(user.getPassword());
			user.getPhones().stream().forEach((Phone phone) -> {
				PhoneEntity phoneEntity = new PhoneEntity();
				phoneEntity.setCitycode(phone.getCityCode());
				phoneEntity.setContrycode(phone.getContryCode());
				phoneEntity.setNumber(phone.getNumber());
				listaPhoneEntity.add(phoneEntity);

			});
			if (!registrationUserRepository.existsByName(user.getName(), user.getPassword())) {
				usuarioEntity.setToken(Utils.generarToken(user));
				usuarioEntity.setEmail(user.getEmail());
				usuarioEntity.setIsactive(true);
				usuarioEntity.setCreated(
						dt1.parse(new SimpleDateFormat(Constants.FORMATO_FECHA).format(Utils.fechaRegistro())));
				usuarioEntity.setLast_login(
						dt1.parse(new SimpleDateFormat(Constants.FORMATO_FECHA).format(Utils.fechaRegistro())));

				response.setMessage(Constants.SUCCESS);
				response.setStatus(Constants.STATE_CREATED);

			} else {
				usuarioEntity.setModified(
						dt1.parse(new SimpleDateFormat(Constants.FORMATO_FECHA).format(Utils.fechaRegistro())));
				List<UsuarioEntity> list = registrationUserRepository.findUser(user.getName(), user.getPassword());
				list.forEach((UsuarioEntity usr) -> {
					try {

						usuarioEntity.setEmail(usr.getEmail() != user.getEmail() ? user.getEmail() : usr.getEmail());
						usuarioEntity.setToken(usr.getToken());
						usuarioEntity.setCreated(
								dt1.parse(new SimpleDateFormat(Constants.FORMATO_FECHA).format(usr.getCreated())));
						usuarioEntity.setId(usr.getId());
						usuarioEntity.setModified(dt1.parse(new SimpleDateFormat(Constants.FORMATO_FECHA).format(Utils.fechaRegistro())));
						usuarioEntity.setLast_login(
								dt1.parse(new SimpleDateFormat(Constants.FORMATO_FECHA).format(usr.getLast_login())));
						
						

					} catch (ParseException e) {

						logger.error(InfoEnumUtil.FIN_REGISTRA_USUARIO_SERVICE.getCode(), e.getLocalizedMessage());

					}
				});
				response.setMessage(Constants.SUCCESS);
				response.setStatus(Constants.STATUS_MODIFIED);

			}

			usuarioEntity.setPhones(listaPhoneEntity);
			registrationUserRepository.save(usuarioEntity);
			data.setId(usuarioEntity.getId() != null ? usuarioEntity.getId().toString() : "");
			data.setCreated(usuarioEntity.getCreated() != null ? usuarioEntity.getCreated().toString() : "");
			data.setModified(usuarioEntity.getModified() != null ? usuarioEntity.getModified().toString() : "");
			data.setLast_login(usuarioEntity.getLast_login() != null ? usuarioEntity.getLast_login().toString() : "");
			data.setToken(usuarioEntity.getToken() != null ? usuarioEntity.getToken().toString() : "");
			data.setIsactive(usuarioEntity.isIsactive()?Constants.ACTIVE_USER:Constants.INACTIVE_USER);
		
			logger.info(InfoEnumUtil.FIN_REGISTRA_USUARIO_SERVICE.getCode(), usuarioEntity.toString());

		} catch (DataAccessException ex) {
			logger.error(InfoEnumUtil.FIN_REGISTRA_USUARIO_SERVICE.getCode(),ex.getLocalizedMessage());

			response.setMessage(Constants.FAIL);
			response.setStatus(Constants.STATUS_FAIL);
			throw new BusinessException(ErrorEnumUtil.ERROR_REGISTRAR_USUARIO);

		}

		response.setData(data);

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_REGISTRA_USUARIO_SERVICE.getCode(),
				response);

		return response;
	}

}
