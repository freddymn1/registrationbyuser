package com.registration.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@Entity
@Getter
@Setter
@ToString
@Table(name = "PHONE")

public class PhoneEntity {

	@Id
	@GeneratedValue
	int id;

	@Column(name = "number", nullable = true)
	private String number;

	@Column(name = "citycode", nullable = true)
	private String citycode;

	@Column(name = "contrycode", nullable = true)
	private String contrycode;

}
