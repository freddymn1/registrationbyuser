package com.registration.controller;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.registration.dto.RegistrationUserResponse;
import com.registration.dto.GenericDtoResponse;
import com.registration.dto.UserRequest;
import com.registration.service.RegistrationUserService;
import com.registration.utils.InfoEnumUtil;
import com.registration.utils.LogMessageUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;

/**
 * Clase para registrar usuarios
 * 
 * @author fpainenao
 * 
 */
@RestController
@Api(value = "RegistrationController")
public class RegistrationController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

	@Autowired
	private RegistrationUserService registrationUserService;

	/**
	 * Metodo encargado de registrar usuario
	 * 
	 * @param UserRequest
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Registra Usuarios")
	@PostMapping("/registrar/usuario")
	@ApiResponses(value = { @ApiResponse(code = 201, message = " Usuario Registrado con éxito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> saveUser(@RequestBody @Valid UserRequest request) throws ParseException {
		
		logger.info("saveUser input {}", request);
		
		RegistrationUserResponse lDtor = registrationUserService.saveUpUser(request);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_REGISTRA_USUARIO.getCode(),
				"/registrar/usuario", LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

}
