package com.registration.controller;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.registration.dto.GenericDtoResponse;


public class BaseController {

    @Autowired
    private HttpServletRequest req;

    /**
     * Retorna 201 y su location para el recurso creado
     * 
     * @param id
     * @return
     */
    public ResponseEntity<Void> returnCreateHttp(int id) {
	URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();

	return ResponseEntity.created(location).build();
    }

    /**
     * Retorna 201 y SIN LOCATION
     * 
     * @param id
     * @return
     */
    public ResponseEntity<Void> returnCreateHttpWithOutLocation() {
	return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Retorna 204
     * 
     * @return
     */
    public ResponseEntity<Void> returnDeleteHttp() {
	return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Retorna 200 con body
     * 
     * @param result
     * @return
     */
    public ResponseEntity<GenericDtoResponse> returnSuccessHttp(Object result) {
	GenericDtoResponse r = new GenericDtoResponse(result, "OK");
	return new ResponseEntity<>(r, HttpStatus.OK);
    }
    

}
