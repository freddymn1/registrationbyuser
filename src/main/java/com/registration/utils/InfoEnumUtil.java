package com.registration.utils;

public enum InfoEnumUtil {

	INI_REGISTRA_USUARIO("001-INI REGISTRATION CONTROLLER ",
			"Inicio controller regsitro de usuario"),
	
	FIN_REGISTRA_USUARIO("002-FIN REGISTRATION CONTROLLER ",
			"FIN controller regsitro de usuario\""),

	INI_REGISTRA_USUARIO_SERVICE("003-INI REGISTRATION SERVICE","INI Services Registra usuario"),
	
	FIN_REGISTRA_USUARIO_SERVICE("004-FIN REGISTRATION SERVICE","FIN Services Registra usuario");

	
	private final String code;
	private final String msg;

	InfoEnumUtil(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return this.code;
	}

	public String getMsg() {
		return this.msg;
	}
}