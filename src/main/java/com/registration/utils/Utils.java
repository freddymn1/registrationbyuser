package com.registration.utils;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import com.registration.dto.UserRequest;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Utils implements Serializable {

	private static final long serialVersionUID = 6808461961003758547L;


	public Utils() {
		throw new IllegalStateException("Utility class");
	}

	public static Date fechaRegistro() {

		LocalDate ld = LocalDate.now();
		Instant instant = ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		Date res = Date.from(instant);
		return res;
	}

	public static String generarToken(UserRequest user) {

		long tiempo = System.currentTimeMillis();
		String token = Jwts.builder().signWith(SignatureAlgorithm.HS256, Constants.KEYTOKEN).setSubject("fpainenao")
				.setIssuedAt(new Date(tiempo)).setExpiration(new Date(tiempo + 900000))
				.claim(Constants.CLAIMTOKEN, user.getEmail()).compact();

		return token;
	}

}
