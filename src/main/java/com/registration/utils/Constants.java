package com.registration.utils;

import java.io.Serializable;

public class Constants implements Serializable {

	private static final long serialVersionUID = 6354630435599385052L;

	public Constants() {
		throw new IllegalStateException("Utility class");
	}
	public static final String FORMATO_FECHA = "dd/MM/yyyy";
	public static final String MAIL_ALREADY_REGISTERED = "Mail already exists";
	public static final String SUCCESS = "Successfully Registered";
	public static final int STATUS_OK = 200;
	public static final int STATE_CREATED = 201;
	public static final int STATUS_MODIFIED = 200;
	public static final String ACTIVE_USER = "Activo";
	public static final String INACTIVE_USER = "No Activo";
	public static final String FAIL = "Proceso Fallido";
	public static final int STATUS_FAIL = 409;
	public static final String KEYTOKEN = "miclave";
	public static final String CLAIMTOKEN = "email";
	
	
	
	
}
