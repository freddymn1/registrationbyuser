# Registration User

Integracion Registra usuarios


#### Control de versiones 
| Version | Etapa      | Fecha      |
| ------- | ---------- | ---------- |
| 1.0     | Desarrollo | 12-10-2020 |
|

### Aspectos Técnicos

Diseñado con las siguientes tecnologías:

* [Spring Boot] Microservicio REST
* [Hibernate JPA] Persistencia
* [H2] Base de Datos
* [lombok] Ultima Version.

### Configuracion de aplicacion

El servicio necesita configuraciones propias de la aplicación y que se configuren los datos necesarios para hacer conexión a la base de datos del ambiente correspondiente . Por lo cual, se definen las siguientes variables de entorno.

Para la configuracion lombok agregar plug in a ide.
Se agrega configuracion basica para el uso Health Check

##### Configuracion de Ambiente
Definidas en application.properties 


##### Configuracion Openshift Editar en YAML

| Servicio | URL      |
| ------- | ---------- |
|http://localhost:8080/swagger-ui.html
 
 
